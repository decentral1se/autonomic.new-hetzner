# autonomic.new-hetzner

> https://autonomic.zone/

Bootstrap new Hetzner VPSes with Ansible.

This role has been copied out of our internal role repository (all roles
GPLv3'd but they sit alongside our password credentials, so we keep it private)
to show the Ansible code.
